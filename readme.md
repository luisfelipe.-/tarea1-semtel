## Tarea 1
***Bastian Cornejo    201730003-5***
***Kattya Herrera     201730007-8***
***Luis Torres        201730025-6***
# **Agente revelador de espacios de Buscaminas**

Este agente utilizó sobre un [buscaminas hecho en Python](https://www.askpython.com/python/examples/create-minesweeper-using-python) con las mismas cualidades del juego original, pero con variables manejables directamente a partir del código, como el conocimiento de la posición de las minas y el movimiento a través del grid.

**El objetivo es revelar todas las posiciones del grid que no tienen minas a partir de una posición aleatoria dentro de este.** 
Para esto se utilizaron tres métodos distintos:
  - UCS
  - Greedy
  - A*

Al final de la ejecución del agente se mostrará el tiempo de ejecución con el fin de comparar estos entre los distintos métodos.
## **Ejecución**
Se trabajó sobre el editor Visual Studio Code y usando el IDLE de Python 3.x. El trabajo además se encuentra en [GitLab](https://gitlab.com/luisfelipe.-/tarea1-semtel) donde se puede clonar o descargar.
Se realizarán 10 pruebas con cada método, donde luego se obtendrá un promedio del tiempo de ejecución y pasos.
### Usando A*
- n1 = 3.5429329872131348 seconds --- 42 steps
- n2 = 3.1805665493011475 seconds --- 44 steps
- n3 = 3.1295886039733887 seconds --- 44 steps
- n4 = 2.8559634685516357 seconds --- 38 steps
- n5 = 3.448185443878174 seconds --- 44 steps
- n6 = 3.2001209259033203 seconds --- 44 steps
- n7 = 3.1237940788269043 seconds --- 44 steps
- n8 = 2.9550771713256836 seconds --- 39 steps
- n9 = 3.533592700958252 seconds --- 44 steps
- n10 = 3.2201881408691406 seconds --- 44 steps

### Usando UCS
- n1 = 3.422449827194214 seconds --- 44 steps
- n2 = 3.4130961894989014 seconds --- 41 steps
- n3 = 3.846923589706421 seconds --- 44 steps
- n4 = 4.053331136703491 seconds --- 44 steps
- n5 = 3.923365354537964 seconds --- 43 steps
- n6 = 3.7528889179229736 seconds --- 39 steps
- n7 = 4.056878566741943 seconds --- 43 steps 
- n8 = 4.213538646697998 seconds --- 44 steps 
- n9 = 3.5344929695129395 seconds --- 44 steps
- n10 = 3.9576570987701416 seconds --- 44 steps

### Usando Greedy
- n1 = 3.804219961166382 seconds --- 44 steps
- n2 = 2.826950788497925 seconds --- 39 steps
- n3 = 2.981064796447754 seconds --- 40 steps
- n4 = 3.3958077430725098 seconds --- 44 steps
- n5 = 3.596101760864258 seconds --- 44 steps
- n6 = 3.1080050468444824 seconds --- 42 steps
- n7 = 2.823286533355713 seconds --- 38 steps
- n8 = 3.6006925106048584 seconds --- 41 steps
- n9 = 3.7384746074676514 seconds --- 44 steps
- n10 = 3.219470500946045 seconds ---44 steps


## **Conclusiones**
Luego de ejecutar los algoritmos obtuvimos los valores mostrados antes y además se calculó el promedio del número de pasos y de tiempo de ejecución lo que nos permitió concluir que el algoritmo que resuelve el mapa de manera óptima es A-star entregando tiempos de resolución más cortos, mientras que por el contrario aquella que peor resuelve el mapa es UCS pues sus tiempos más largos así lo reflejan. Ahora, donde no se puede concluir nada específico es la relación algoritmo -  número de pasos debido a sus valores tan cercanos entre sí. Como grupo concluimos que esto es debido a que buscaminas no libera un número de casillas constante para cada click. Todo dependerá de la disposición de las minas en el mapa.


| Método        | Tiempo promedio de ejecución en segundos      | Promedio de pasos  |
| :-------------: |:-------------:| :-----:|
| UCS      | 3.81746222973 | 43 |
| Greedy      | 3.30940742493     |   42 |
| A*      | 3.21900100708      |   42.7 |
